package com.cognizant.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecycleManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecycleManagementSystemApplication.class, args);
	}

}
