package com.cognizant.miniproject.dao;

import java.util.List;

import com.cognizant.miniproject.model.BuyerRequest;

public interface BuyerRequestDao {
	
	public List<BuyerRequest> viewBuyerOrders();
	
	public int update(int requestId, String status);

}
