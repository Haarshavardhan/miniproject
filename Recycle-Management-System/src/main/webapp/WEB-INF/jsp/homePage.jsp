<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Recycle Management System</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<style>
body, html {
	height: 100%;
}

body {
	background-image: url("/images/recycle.jpg");
	height: 100%;
	background-repeat: no-repeat;
	background-size: cover;
}
.jumbo{
background: none;
}
</style>
</head>
<body>
	<div>
		<div class="container content">
			<div style="background: rgb(0, 0, 0, 0);"  id="jumbo" class="jumbotron heading">
				<h1 style="color: black; text-align: center">Recycle Management
					System</h1>
			</div>
			<div class="col" style="margin: auto; padding-top: 50px;">
				<a class="col-sm-4 btn btn-basic" role="button"
					href="/adminHomePage"><h3>
						<b>WELCOME ADMIN</b>
					</h3></a> <a class="col-sm-4 btn btn-basic" role="button" href="/manager"><h3>
						<b>WELCOME MANAGER</b>
					</h3></a> <a class="col-sm-4 btn btn-basic" role="button"
					href="/userHomePage"><h3>
						<b>WELCOME USER</b>
					</h3></a>
			</div>
		</div>
	</div>
	<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>